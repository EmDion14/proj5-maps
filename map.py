import flask
from flask import render_template
from flask import request
from flask import url_for

import logging

###
# Globals
###
app = flask.Flask(__name__)
import CONFIG

import uuid
app.secret_key = str(uuid.uuid4())
app.debug=CONFIG.DEBUG
app.logger.setLevel(logging.DEBUG)

def readPOIFile():
    fo = open("POI.txt", 'r')
    lines = fo.read().split('\n')
    fo.close()
    for i in range(len(lines)):
        lines[i] = [float(lines[i].split(' ')[0]), float(lines[i].split(' ')[1]), str(' '.join(lines[i].split(' ')[2:]))]
        
    
    return lines

###
# Pages
###

@app.route("/")
@app.route("/index")
def index(pois=readPOIFile()):
  app.logger.debug("Main page entry")
  return flask.render_template('map.html', pois=pois)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] =  flask.url_for("map")
    return flask.render_template('page_not_found.html'), 404


#############


if __name__ == "__main__":
    import uuid
    app.secret_key = str(uuid.uuid4())
    app.debug=CONFIG.DEBUG
    app.logger.setLevel(logging.DEBUG)
    app.run(port=CONFIG.PORT)
