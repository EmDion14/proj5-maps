# proj5-maps

## Author
Emmalie Dion

Bitbucket repository link: https://EmDion14@bitbucket.org/EmDion14/proj5-maps.git
ix path: home/users/emmalie/proj5-maps


## Program

This program displays a map centered on Eugene, OR. Once the map opens the user can click anywhere on the map and it will create a marker and a popup. When the popup is open it will display the street address of the marker. There will also be three points of interest: Autzen Stadium, VooDoo Donuts, and Spencer's Butte. Each will be marked by a marker and upon clicking a popup will appear with the address and description of the point of interest.


